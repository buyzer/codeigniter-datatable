 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Muser extends CI_Model{  
  function __construct(){
    parent::__construct();
  }
  function getCountUser(){
    $this->db->select("count(user_id) as total");
    $this->db->from("tb_user");
    $q = $this->db->get()->row();
    return $q->total;
  }
  public function datatableUser(){
    $array = array("user_id","name","email","roles","status");
    $start = $this->_post('start');
    $offset = $this->_post('length');
    if ($start != null && $offset != null) {
      $this->db->limit($offset,$start);
    }

    $search = $this->_post('search');
    if($search['value'] != ''){
      $key = $search['value'];
      $this->db->group_start();
      $this->db->like('user_id' , $key);
      $this->db->or_like('name' , $key);
      $this->db->or_like('email', $key);
      $this->db->or_like('username' , $key);
      $this->db->group_end();
    }

    $order = $this->_post('order');
    $column = isset($order[0]['column'])?$order[0]['column']:-1;
    if($column >= 0 && $column < count($array)){
      $ord = $array[$column];
      $by = $order[0]['dir'];
      $this->db->order_by($ord , $by);
    }

    $this->db->select("SQL_CALC_FOUND_ROWS user.*" ,FALSE);
    $this->db->from("tb_user as user");
    $this->db->where("roles <","4");
    $sql = $this->db->get();
    $q = $sql->result();
    $this->db->select("FOUND_ROWS() AS total_row");
    $row = $this->db->get()->row();

    $sEcho = $this->_post('draw');
    $getCountAll = $this->getCountUser();
    $output = array(
      "draw" => intval($sEcho),
      "recordsTotal" => $getCountAll,
      "recordsFiltered" => $row->total_row,
      "data" => array()
    );

    foreach ($q as $user) {
      $btn = '<a href="'.site_url('admin/users/add/'.$user->user_id).'" class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-pencil"></i> Edit</a>
      <a href="#" data-id="'.$user->user_id.'" data-action="'.site_url('admin/users/delete').'" class="btn btn-sm btn-danger my-btn-delete"><i class="fa fa-times"></i> Delete</a>';

      $output['data'][] = array(
        $user->user_id,
        $user->date,
        $user->name,
        $user->email,
        $user->username,
        $user->roles,
        $user->status,
        $btn
      );
    }
    return $output;
  }
}
